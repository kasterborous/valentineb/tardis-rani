local PART={}
PART.ID = "ranipower"
PART.Name = "rani power"
PART.Model = "models/doctormemes/rani/rewrite/power.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Sound = "doctormemes/rani/rewrite/RaniButton1.wav"
PART.Control = "power"
PART.ShouldTakeDamage = true

if SERVER then

	function PART:Think()

	local power=self.exterior:GetData("power-state")
	local interior=self.interior
	local screen=self.interior:GetPart("ranimonitor")
	local warning=self.exterior:GetData("health-warning")
		
		if	power == false then
			interior:SetSubMaterial(1 , "models/doctormemes/rani/rewrite/whiteglow off")
			screen:SetSubMaterial(0 , "models/doctormemes/rani/rewrite/glass")
		else

			if warning == true then
				interior:SetSubMaterial(1 , "models/doctormemes/rani/rewrite/whiteglow warn")
			else
				interior:SetSubMaterial(1 , "models/doctormemes/rani/rewrite/whiteglow")
			end

			screen:SetSubMaterial(0 , "models/doctormemes/rani/rewrite/scanner")
		end
	end
end


TARDIS:AddPart(PART,e)

