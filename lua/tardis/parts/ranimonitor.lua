local PART={}
PART.ID = "ranimonitor"
PART.Name = "Rani's Tardis Monitor"
PART.Model = "models/doctormemes/rani/rewrite/monitor.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.UseTransparencyFix = true
PART.ShouldTakeDamage = true

if SERVER then

	function PART:Think()
   		local exterior=self.exterior
		if exterior:GetData("vortex") then
		self:SetMaterial("models/doctormemes/rani/rewrite/scanner")
		else

		self:SetMaterial("models/doctormemes/rani/rewrite/glass")
		end
	end
	
end

TARDIS:AddPart(PART)