local PART={}
PART.ID = "ranimanual"
PART.Name = "rani manual"
PART.Model = "models/doctormemes/rani/rewrite/manual.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.ShouldTakeDamage = true
PART.Sound = "doctormemes/rani/rewrite/manual.wav"
PART.Control = "cloak"

TARDIS:AddPart(PART)