local PART={}
PART.ID = "raniconsole"
PART.Name = "Rani's TARDIS Console"
PART.Model = "models/doctormemes/rani/rewrite/console.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.ShouldTakeDamage = true
PART.BypassIsomorphic = true
PART.Control = "thirdperson"

TARDIS:AddPart(PART,e)