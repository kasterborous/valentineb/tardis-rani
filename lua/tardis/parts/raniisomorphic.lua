local PART={}
PART.ID = "raniisomorphic"
PART.Name = "rani isomorphic"
PART.Model = "models/doctormemes/rani/rewrite/isomorphic.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Sound = "doctormemes/rani/rewrite/RaniButton1.wav"
PART.Control = "isomorphic"
PART.ShouldTakeDamage = true

TARDIS:AddPart(PART)