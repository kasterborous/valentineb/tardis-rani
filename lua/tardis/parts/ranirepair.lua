local PART={}
PART.ID = "ranirepair"
PART.Name = "rani repair"
PART.Model = "models/doctormemes/rani/rewrite/repair.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Sound = "doctormemes/rani/rewrite/RaniButton1.wav"
PART.Control = "repair"
PART.ShouldTakeDamage = true

TARDIS:AddPart(PART)