--Rani's TARDIS Interior - Control Sequences (advanced mode)

local Seq = {
    ID = "rani_sequences",

    ["ranicoordinates"] = {
        Controls = {
            "ranihelmic",
            "raniphyslock",
            "ranilongflight",
            "ranihandbrake",
	    "ranithrottle"
        },
        OnFinish = function(self)
		if self.exterior:GetData("vortex") then
            		if IsValid(self) and IsValid(self) then
               			self.exterior:Mat()
            		end
      		end
		if not self.exterior:GetData("vortex") then
		        if IsValid(self) and IsValid(self) then
				self.exterior:Demat()
			end
		end
	end
    },
	["ranivortex"] = {
        Controls = {
            "ranihelmic",
            "raniphyslock",
            "ranilongflight",
            "ranihandbrake",
	    "ranithrottle"
        },
        OnFinish = function(self)
		if self.exterior:GetData("vortex") then
            		if IsValid(self) and IsValid(self) then
               			self.exterior:Mat()
            		end
      		end
		if not self.exterior:GetData("vortex") then
		        if IsValid(self) and IsValid(self) then
				self.exterior:Demat()
			end
		end
	end
    }
}

TARDIS:AddControlSequence(Seq)