-- Rani's TARDIS

local T={}
T.Base="base"
T.Name="Rani's TARDIS"
T.ID="rani"
T.Interior={
	Model="models/doctormemes/rani/rewrite/interior.mdl",
	Portal={
	        pos=Vector(-235.72,61,58),
	        ang=Angle(0,-90,0),
		width=56,
		height=107
	},
	Fallback={
		pos=Vector(-235,44,15),
		ang=Angle(0,-90,0),
	},
	ExitDistance=300,

	Sequences="rani_sequences",

	Parts={
                door={
                        model="models/doctormemes/rani/rewrite/intdoor.mdl",posoffset=Vector(17.79,0,-49.8),angoffset=Angle(0,-180,0)
                },
		raniconsole=true,
		raniaudiosystem=true,
		ranicoordinates=true,
		ranifastreturn=true,
		raniflightmode=true,
		ranihads=true,
		ranihandbrake=true,
		ranihelmic=true,
		raniisomorphic=true,
		ranilongflight=true,
		ranimanual=true,
		raniphyslock=true,
		ranipower=true,
		ranirepair=true,
		ranirotor=true,
		raniscanner=true,
		ranithrottle=true,
		ranivortex=true,
		ranimonitor=true,

	},
	IdleSound={
		{
			path="doctormemes/rani/rewrite/interior_idle_loop.wav",
			volume=1	
		}
	},
	Light={
	      color=Color(255,255,255),
	      warncolor=Color(255,0,0),
	      pos=Vector(0,0,150),
	      brightness=2
	},
	TipSettings={
            		view_range_max=70,
            		view_range_min=65,
			style="rani_tips",
	},
	PartTips={
        ranithrottle	=	{pos=Vector(21.1561 , 0 , 50.9441 ),text="Demat Switch"},
        ranihandbrake	=	{pos=Vector(30.2496 , -8.09305 , 46.8744 ),text="Handbrake"},
        ranilongflight	=	{pos=Vector(-8.67667 , -19.29503 , 50.94411 ),text="Vortex Flight"},
        ranicoordinates	=	{pos=Vector(-11.4376 , -31.2162 , 44.8329 ),text="Coordinates"},
        raniphyslock	=	{pos=Vector(-8.15633 , 30.2326 , 46.8744 ),text="Physical Lock"},
        ranihelmic	=	{pos=Vector(-18.4093 , 27.6833 , 44.9411 ),text="Fast Return"},
		ranivortex	=	{pos=Vector(31.475, 8.144, 47.147 ),text="Redecoration", right = true},
		raniscanner	=	{pos=Vector(16.028, -22.823, 47.687 ),text="Manual Destination Selection", down = true},
		raniaudiosystem	=	{pos=Vector(-28.917, 0.11, 46.539 ),text="Music", down = true},
	},
	CustomTips={
		{pos=Vector(32.7591 , 3.91093 , 43.3321 ),     text="Cloaking Device", down = true, right = true},
		{pos=Vector(28.346 , -17.3284 , 44.6433 ),     text="Repair"},
		{pos=Vector(-19.78723 , -24.26935 , 46.87442 ),     text="H.A.D.S."},
		{pos=Vector(-22.22165 , 22.08696 , 46.87004 ),     text="Flight", right = true},
		{pos=Vector(-10.6025 , 18.3076 , 50.9441 ),     text="Float"},
		{pos=Vector(14.7548 , 23.7294 , 46.4173 ),     text="Power"},
		{pos=Vector(-4.96335 , -30.9354 , 46.87 ),     text="Security", right = true},
	},
}
T.Exterior={
	Model="models/doctormemes/rani/rewrite/exteriorrewrite.mdl",
	Mass=5000,
	Portal={
		pos=Vector(17.8,-0.004,49.8),
		ang=Angle(0,0,0),
		width=39,
		height=88
	},
	Fallback={
		pos=Vector(44,0,7),
		ang=Angle(0,0,0)
	},
	Light={
		enabled=false,
		pos=Vector(0,0,109),
		color=Color(255,240,160)
	},
	Sounds={
		Lock="doctormemes/rani/rewrite/doorlock.wav",
		Door={
			enabled=true,
			open="doctormemes/rani/rewrite/door.wav",
			close="doctormemes/rani/rewrite/door.wav"
		}
	},
	Parts={
                door={
                        model="models/doctormemes/rani/rewrite/extdoor.mdl",posoffset=Vector(-17.79,0,-49.8),angoffset=Angle(0,0,0)
                },
		vortex={
			model="models/doctormemes/rani/rewrite/vortex.mdl",
			pos=Vector(0,0,50),
			ang=Angle(0,90,0),
			scale=10		
		}
	},
	Teleport={
		SequenceSpeed=0.77,
		SequenceSpeedFast=0.935,
		DematSequence={
			175,
			230,
			100,
			150,
			50,
			100,
			0
		},
		MatSequence={
			100,
			50,
			150,
			100,
			200,
			150,
			255
		}
	}
}

TARDIS:AddInterior(T)