local style={
	style_id="rani_tips",
	style_name="Rani",
	font = "Trebuchet24",
	colors = {
		normal = {
			text = Color( 255, 255, 255, 255 ),
			background = Color( 160, 40, 10, 220 ),
			frame = Color( 30, 30, 30, 230 ),
		},
		highlighted = {
			text = Color( 255, 255, 255, 255 ),
			background = Color( 40, 160, 10, 220 ),
			frame = Color( 30, 30, 30, 230 ),
		}
	}
}
TARDIS:AddTipStyle(style)